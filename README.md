# Tests for Platform and Station

Suite of Arduino sketches to test Platform and Station devices. Each folder contains a single Arduino sketch.

## Contents

- [Terminology](#terminology)
- [Test List](#testlist)
- [Authors](#authors)

## Terminology

- **Platform:** onboard system consisted of **Teensy 3.5**, **Adafruit BNO055**, and **Adafruit RF69HCW**.
- **Station:** transceiver **Adafruit Feather M0** attached to the user's computer.

## Test List

If tests are performed in pairs, it is recommended to associate the following tests:

- **Debug_lib_platform** and **Debug_lib_station**
- **Feather_Radio_RX** and **Teensy_Radio_TX**
- **Feather_Radio_RX_Packets** and **Teensy_Radio_TX_Packets**
- **Feather_Radio_RX_Packets** and **Teensy_full**

|           Test           |  Target  |    Depth     |                           Description                            |
| ------------------------ | -------- | ------------ | ---------------------------------------------------------------- |
| Debug_lib_platform       | Platform | total        | Compile `Platform.h` into Teensy and test in real environment    |
| Debug_lib_station        | Station  | total        | Compile `Station.h` into Feather M0 and test in real environment |
| Feather_Radio_RX         | Station  | shallow      | Receive raw messages from Platform                               |
| Feather_Radio_RX_Packets | Station  | deep         | Receive packetized messages from Platform                        |
| Feather_Radio_TX         | Station  | shallow      | Transmit raw messages from Platform                              |
| Teensy_full              | Platform | very deep    | Switch on both IMU and Radio                                     |
| Teensy_IMU_Data          | Platform | shallow      | Get IMU orientation data via Serial                              |
| Teensy_IMU_Status        | Platform | shallow      | Get IMU status and calib data via Serial                         |
| Teensy_Radio_TX          | Platform | shallow      | Transmit raw messages to Station                                 |
| Teensy_Radio_TX_Packets  | Platform | deep         | Transmit packetized messages to Station                          |
| Teensy_Serial            | Platform | very shallow | Test USB Serial communications                                   |

## Authors

- David Criado Pernía
