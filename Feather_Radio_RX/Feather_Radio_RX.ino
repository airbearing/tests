/****************************************************************************
 * Air Bearing
 * David Criado Pernia
 * 2019
 * 
 * TEST
 * Feather M0 as a Radio receiver of raw data
 *
 * Example sketch showing how to create a simple messageing client
 * with the RH_RF69 class. RH_RF69 class does not provide for addressing or
 * reliability, so you should only use RH_RF69  if you do not need the higher
 * level messaging abilities.
 * It is designed to work with the other example rf69_server.
 * Demonstrates the use of AES encryption, setting the frequency and modem
 * configuration
 * 
 ***************************************************************************/

//////////////////////////////////////////////////////////
// Libraries
//////////////////////////////////////////////////////////

#include <SPI.h>                    // Arduino
#include <RH_RF69.h>                // Third-party

//////////////////////////////////////////////////////////
// Constants
//////////////////////////////////////////////////////////

///////// Transceiver
#define RF69_FREQ (433.0)           //!< Transceiver Frequency [Hz]

///////// Feather M0
#define RFM69_CS        (8)         //!< Feather M0 pin to transceiver SPI chip select pin
#define RFM69_INT       (3)         //!< Feather M0 pin to transceiver SPI interrupt pin
#define RFM69_RST       (4)         //!< Feather M0 pin to transceiver SPI reset pin
#define LED             (13)        //!< Feather M0 LED pin

//////////////////////////////////////////////////////////
// Class declaration
//////////////////////////////////////////////////////////

RH_RF69 rf69(RFM69_CS, RFM69_INT);  //<! Instance of radio driver

//////////////////////////////////////////////////////////
// Program Setup
//////////////////////////////////////////////////////////

int16_t packetnum = 0;              //<! Packet counter

void setup() 
{
  Serial.begin(115200);
  while (!Serial)
  {
    digitalWrite(LED, HIGH);
    delay(200);
    digitalWrite(LED, LOW);
    delay(200);
  }

  pinMode(LED, OUTPUT);     
  pinMode(RFM69_RST, OUTPUT);
  digitalWrite(RFM69_RST, LOW);

  Serial.println("Feather RFM69 RX Test!");
  Serial.println();

  // Manual radio reset
  digitalWrite(RFM69_RST, HIGH);
  delay(10);
  digitalWrite(RFM69_RST, LOW);
  delay(10);
  
  if (!rf69.init())
  {
    Serial.println("RFM69 radio init failed");
    while (1);
  }
  Serial.println("RFM69 radio init OK!");
  
  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM (for low power module)
  // No encryption
  if (!rf69.setFrequency(RF69_FREQ))
  {
    Serial.println("setFrequency failed");
  }

  // If you are using a high power RF69 eg RFM69HW, you *must* set a Tx power with the
  // ishighpowermodule flag set like this:
  rf69.setTxPower(20, true);  // range from 14-20 for power, 2nd arg must be true for 69HCW

  // The encryption key has to be the same as the one in the server
  uint8_t key[] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
  rf69.setEncryptionKey(key);
  
  pinMode(LED, OUTPUT);

  Serial.print("RFM69 radio @");  Serial.print((int)RF69_FREQ);  Serial.println(" MHz");
}

//////////////////////////////////////////////////////////
// Program Loop
//////////////////////////////////////////////////////////

void loop()
{
 if (rf69.available())
 {
    // Should be a message for us now   
    uint8_t buf[RH_RF69_MAX_MESSAGE_LEN];
    uint8_t len = sizeof(buf);
    if (rf69.recv(buf, &len))
    {
      if (!len) return;
      buf[len] = 0;
      Serial.print("Received [");
      Serial.print(len);
      Serial.print("]: ");
      Serial.println((char*)buf);
      Serial.print("RSSI: ");
      Serial.println(rf69.lastRssi(), DEC);

      if (strstr((char *)buf, "Hello World"))
      {
        // Send a reply!
        uint8_t data[] = "And hello back to you";
        rf69.send(data, sizeof(data));
        rf69.waitPacketSent();
        Serial.println("Sent a reply");
        blink(LED, 40, 3); // blink LED 3 times, 40ms between blinks
      }
    }
    else
    {
      Serial.println("Receive failed");
    }
  }
}

//! Blink pin
void blink(byte PIN, byte DELAY_MS, byte loops)
{
  for (byte i=0; i<loops; i++)
  {
    digitalWrite(PIN,HIGH);
    delay(DELAY_MS);
    digitalWrite(PIN,LOW);
    delay(DELAY_MS);
  }
}
