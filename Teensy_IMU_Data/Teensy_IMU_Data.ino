/****************************************************************************
 * Air Bearing
 * David Criado Pernia
 * 2019
 * 
 * TEST
 * Teensy and IMU data
 *
 * Prompts IMU data through USB serial port.
 * 
 * Possible vector values can be:
 * - VECTOR_ACCELEROMETER - m/s^2
 * - VECTOR_MAGNETOMETER  - uT
 * - VECTOR_GYROSCOPE     - rad/s
 * - VECTOR_EULER         - degrees
 * - VECTOR_LINEARACCEL   - m/s^2
 * - VECTOR_GRAVITY       - m/s^2
 * 
 *                            Teensy I2C Setup
 *         ---------------------------------------------------------        
 *         Interface  Devices     Pin Name      SCL    SDA   Default
 *         ---------  -------  --------------  -----  -----  -------
 *            Wire      All    I2C_PINS_16_17    16     17
 *            Wire      All    I2C_PINS_18_19    19*    18      +  
 *            Wire    3.5/3.6  I2C_PINS_7_8       7      8
 *            Wire    3.5/3.6  I2C_PINS_33_34    33     34
 *            Wire    3.5/3.6  I2C_PINS_47_48    47     48
 *           Wire1       LC    I2C_PINS_22_23    22     23      +
 *           Wire1    3.1/3.2  I2C_PINS_26_31    26     31
 *           Wire1    3.1/3.2  I2C_PINS_29_30    29     30      +
 *           Wire1    3.5/3.6  I2C_PINS_37_38    37     38      +
 *           Wire2    3.5/3.6  I2C_PINS_3_4       3      4      +
 *           Wire3      3.6    I2C_PINS_56_57    57*    56      +
 * 
 *                          TEENSY 3.5 PINOUT SETUP
 *                       -----------------------------
 *                         RST        OUTPUT    23
 *                         SCK        OUTPUT    18
 *                         SDA        OUTPUT    19
 *
 ***************************************************************************/

//////////////////////////////////////////////////////////
// Libraries
//////////////////////////////////////////////////////////

#include <i2c_t3.h>                               // Instead of #include <Wire.h>. More info: https://goo.gl/RyzFaM
#include <Adafruit_Sensor.h>                      // Unified Sensor Library
#include <Adafruit_BNO055_t3.h>                   // Instead of #include <Adafruit_BNO055.h>. More info: https://goo.gl/eAYKjn
#include <utility/imumaths.h>                     // Required to use vector and quaternion namespace

//////////////////////////////////////////////////////////
// Constants
//////////////////////////////////////////////////////////

#define BNO_RST                     (23)          //!< Teensy's pin to inertial unit **reset** pin
#define LED                         (13)          //!< Teensy's **LED** pin

//////////////////////////////////////////////////////////
// Class declaration
//////////////////////////////////////////////////////////

//! Instance of the IMU driver
/*! 
 * \param[in] iBus I2C Bus
 * \param[in] sensorID Sensor ID 
 * \param[in] aAddress Sensor I2C Address
 * \param[in] iMode I2C Mode (Master/Slave)
 * \param[in] pins I2C Pins
 * \param[in] pullup I2C Pull-ups (Internal/External)
 * \param[in] iRate I2C Rate [Hz]
 * \param[in] opeMode I2C Operational mode
 */
Adafruit_BNO055 bno = Adafruit_BNO055(WIRE_BUS, -1, BNO055_ADDRESS_A, I2C_MASTER, 
                                      I2C_PINS_18_19, I2C_PULLUP_EXT, I2C_RATE_100, I2C_OP_MODE_ISR);

//////////////////////////////////////////////////////////
// Program Setup
//////////////////////////////////////////////////////////

void setup()
{
  Serial.begin(115200);
  while (!Serial)
  {
    digitalWrite(LED, HIGH);
    delay(200);
    digitalWrite(LED, LOW);
    delay(200);
  }
  delay(5000);
  Serial.println("Orientation Sensor Raw Data Test"); Serial.println("");

  /* Initialise the sensor */
  if (!bno.begin())
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    while (1);
  }
  else
  {
    Serial.print("BNO055 detected ... Setting up program");
  }

  delay(1000);

  int8_t temp = bno.getTemp();            // Get current temperature
  Serial.print("Current Temperature: ");
  Serial.print(temp);
  Serial.println(" C");
  Serial.println("");

  setCal();                               // Calibrate IMU
  bno.setExtCrystalUse(true);
  bno.setMode(bno.OPERATION_MODE_NDOF);   // Set IMU Operational mode
}

//////////////////////////////////////////////////////////
// Program Loop
//////////////////////////////////////////////////////////

void loop()
{
  // Print any data (see preamble):
  // imu::Vector<3> data = bno.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER);
  // Serial.print(data[0]); Serial.print("\t");
  // Serial.print(data[1]); Serial.print("\t");
  // Serial.println(data[2]);

  // Print quaternion:
  // imu::Quaternion quat = bno.getQuat();
  // Serial.print("qW: ");  Serial.print(quat.w(), 4);
  // Serial.print(" qX: "); Serial.print(quat.y(), 4);
  // Serial.print(" qY: "); Serial.print(quat.x(), 4);
  // Serial.print(" qZ: "); Serial.print(quat.z(), 4);
  // Serial.println("\t\t");

  // Print Euler angles:
  imu::Quaternion quat = bno.getQuat();
  imu::Vector<3>  euler = quat.toEuler();
  Serial.print(euler[0]); Serial.print("\t");
  Serial.print(euler[1]); Serial.print("\t");
  Serial.println(euler[2]);

  delay(10);
}

//! Calibrate IMU setting predefined offsets
void setCal()
{
  const uint8_t calibrationData[22] = {uint8_t lowByte(65522),uint8_t highByte(65522),
                                    uint8_t lowByte(65499),uint8_t highByte(65499),
                                    uint8_t lowByte(27),   uint8_t highByte(27),
                                                
                                    uint8_t lowByte(65535),uint8_t highByte(65535),
                                    uint8_t lowByte(2),    uint8_t highByte(2),
                                    uint8_t lowByte(1),    uint8_t highByte(1),
                                                
                                    uint8_t lowByte(172),  uint8_t highByte(172),
                                    uint8_t lowByte(65316),uint8_t highByte(65316),
                                    uint8_t lowByte(155),  uint8_t highByte(155),
                                                
                                    uint8_t lowByte(1000), uint8_t highByte(1000),
                                    uint8_t lowByte(475),  uint8_t highByte(475)};
  
  bno.setSensorOffsets(calibrationData);
  delay(4000);
  Serial.println("IMU: offsets added");

  // Check if IMU is calibrated
  if (bno.isFullyCalibrated())
  {
      Serial.println("IMU: fully calibrated");
  } 
}
