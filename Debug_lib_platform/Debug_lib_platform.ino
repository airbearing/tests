/****************************************************************************
 * Air Bearing
 * David Criado Pernia
 * 2019
 * 
 * TEST
 * Code to be compiled in Platform
 *
 * LED BLINK ERROR CODES
 * ==========================================================================
 * Serial port failed                 Continuous blink (5 per second) 
 * Radio begin failed                 1 per second
 * IMU begin failed                   3 per second
 * DATA operational mode without IMU  2 per second
 * 
 *
 * TEENSY 3.5 PINOUT SETUP
 * ==========================================================================
 * LED        OUTPUT    13 (on board)
 * ----- RF ----------------------
 * RST        OUTPUT    34
 * G0 (IRQ)   INPUT     33
 * CS         OUTPUT    15 (CS0)
 * MOSI       -         28 (MOSI0)
 * MISO       -         39 (MISO0)
 * SCK        -         14 (SCK0)
 * EN (NO CONNECTED)
 * GND        -         GND
 * VIN        -         3.3V
 * ----- IMU ----------------------
 * RST        OUTPUT    23
 * SCK        OUTPUT    18
 * SDA        OUTPUT    19
 *
 */

// Include Platform library
#include <platform.h>

// Declare instance of class Platform
// Optional boolean args: Serial (false), Radio (true), IMU (true)
// Args allow to switch USB, TXR or IMU if needed
Platform platform(true, true, true);

// Prepare environment
void setup()
{
     // Init devices, check IMU calibration
     platform.begin();
}

// Run infinitely
void loop()
{
     // Listen if any telecommand (TC) has been sent from station
     // TC are sent as "int A,int B,int C" from serial USB or Radio
     // A: Operating mode, B & C: Parameters
     platform.listen();
     // Operate according to selected Operating Mode and parameters
     platform.operate();
}
