/****************************************************************************
 * Air Bearing
 * David Criado Pernia
 * 2019
 * 
 * TEST
 * Feather M0 as a Radio receiver of packetized data
 *
 * Example sketch showing how to create a simple addressed, reliable messaging client
 * with the RH_RF69 class. RH_RF69 class does not provide for addressing or
 * reliability, so you should only use RH_RF69  if you do not need the higher
 * level messaging abilities.
 * It is designed to work with the other example rf69_server.
 * Demonstrates the use of AES encryption, setting the frequency and modem 
 * configuration
 * 
 ***************************************************************************/

//////////////////////////////////////////////////////////
// Libraries
//////////////////////////////////////////////////////////

#include <SPI.h>                      // SPI
#include <RH_RF69.h>                  // Radio driver
#include <RHReliableDatagram.h>       // Packetized coms

//////////////////////////////////////////////////////////
// Constants
//////////////////////////////////////////////////////////

///////// Transceiver
#define RF69_FREQ       (433.0)       //!< Transceiver Frequency [Hz]
#define MY_ADDRESS      (1)           //!< Transceiver address

///////// Feather M0
#define RFM69_CS        (8)           //!< Feather M0 pin to transceiver SPI chip select pin
#define RFM69_INT       (3)           //!< Feather M0 pin to transceiver SPI interrupt pin
#define RFM69_RST       (4)           //!< Feather M0 pin to transceiver SPI reset pin
#define LED             (13)          //!< Feather M0 LED pin

//////////////////////////////////////////////////////////
// Class declaration
//////////////////////////////////////////////////////////

RH_RF69 rf69(RFM69_CS, RFM69_INT);    //<! Radio driver
RHReliableDatagram rf69_manager(rf69, MY_ADDRESS); //<! Manage message delivery and receipt

//////////////////////////////////////////////////////////
// Program Setup
//////////////////////////////////////////////////////////

int16_t packetnum = 0;                //<! Packet counter
uint8_t data[] = "TM ACK";            //<! telemetry ACK message
uint8_t buf[RH_RF69_MAX_MESSAGE_LEN]; //<! Buffer to receive

void setup() 
{
  Serial.begin(115200);
  while (!Serial)
  {
    digitalWrite(LED, HIGH);
    delay(200);
    digitalWrite(LED, LOW);
    delay(200);
  }

  pinMode(LED, OUTPUT);     
  pinMode(RFM69_RST, OUTPUT);
  digitalWrite(RFM69_RST, LOW);

  Serial.println("Feather Addressed RFM69 RX Test!");
  Serial.println();

  // manual reset
  digitalWrite(RFM69_RST, HIGH);
  delay(10);
  digitalWrite(RFM69_RST, LOW);
  delay(10);
  
  if (!rf69_manager.init())
  {
    Serial.println("RFM69 radio init failed");
    while (1);
  }
  Serial.println("RFM69 radio init OK!");
  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM (for low power module)
  // No encryption
  if (!rf69.setFrequency(RF69_FREQ))
  {
    Serial.println("setFrequency failed");
  }

  // If you are using a high power RF69 eg RFM69HW, you *must* set a Tx power with the
  // ishighpowermodule flag set like this:
  rf69.setTxPower(20, true);  // range from 14-20 for power, 2nd arg must be true for 69HCW

  // The encryption key has to be the same as the one in the server
  uint8_t key[] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
  rf69.setEncryptionKey(key);
  
  pinMode(LED, OUTPUT);

  Serial.print("RFM69 radio @");  Serial.print((int)RF69_FREQ);  Serial.println(" MHz");
}

//////////////////////////////////////////////////////////
// Program Loop
//////////////////////////////////////////////////////////

void loop()
{
  if (rf69_manager.available())
  {
    // Wait for a message addressed to us from the client
    uint8_t len = sizeof(buf);
    uint8_t from;
    if (rf69_manager.recvfromAck(buf, &len, &from))
    {
      buf[len] = 0; // zero out remaining string
      
      Serial.print("Got packet from #"); Serial.print(from);
      Serial.print(" [RSSI :");
      Serial.print(rf69.lastRssi());
      Serial.print("] : ");
      Serial.println((char*)buf);

      // Send a reply back to the originator client
      if (!rf69_manager.sendtoWait(data, sizeof(data), from))
        Serial.println("Sending failed (no ack)");
    }
  }
}
