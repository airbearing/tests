/****************************************************************************
 * Air Bearing
 * David Criado Pernia
 * 2019
 * 
 * TEST
 * Teensy and IMU status
 *
 * Prompts IMU status (health and calib) through USB serial port.
 * 
 * 
 * System Status (see section 4.3.58)
 * ---------------------------------
 * 0 = Idle
 * 1 = System Error
 * 2 = Initializing Peripherals
 * 3 = System Iniitalization
 * 4 = Executing Self-Test
 * 5 = Sensor fusio algorithm running
 * 6 = System running without fusion algorithms
 * 
 * Self Test Results (see section )
 * --------------------------------
 * 1 = test passed, 0 = test failed
 * 
 * Bit 0 = Accelerometer self test
 * Bit 1 = Magnetometer self test
 * Bit 2 = Gyroscope self test
 * Bit 3 = MCU self test
 * 
 * 0x0F = all good!
 * 
 * System Error (see section 4.3.59)
 * ---------------------------------
 * 0 = No error
 * 1 = Peripheral initialization error
 * 2 = System initialization error
 * 3 = Self test result failed
 * 4 = Register map value out of range
 * 5 = Register map address out of range
 * 6 = Register map write error
 * 7 = BNO low power mode not available for selected operat ion mode
 * 8 = Accelerometer power mode not available
 * 9 = Fusion algorithm configuration error
 * A = Sensor configuration error
 * 
 * 
 *                            Teensy I2C Setup
 *         ---------------------------------------------------------        
 *         Interface  Devices     Pin Name      SCL    SDA   Default
 *         ---------  -------  --------------  -----  -----  -------
 *            Wire      All    I2C_PINS_16_17    16     17
 *            Wire      All    I2C_PINS_18_19    19*    18      +  
 *            Wire    3.5/3.6  I2C_PINS_7_8       7      8
 *            Wire    3.5/3.6  I2C_PINS_33_34    33     34
 *            Wire    3.5/3.6  I2C_PINS_47_48    47     48
 *           Wire1       LC    I2C_PINS_22_23    22     23      +
 *           Wire1    3.1/3.2  I2C_PINS_26_31    26     31
 *           Wire1    3.1/3.2  I2C_PINS_29_30    29     30      +
 *           Wire1    3.5/3.6  I2C_PINS_37_38    37     38      +
 *           Wire2    3.5/3.6  I2C_PINS_3_4       3      4      +
 *           Wire3      3.6    I2C_PINS_56_57    57*    56      +
 * 
 *                          TEENSY 3.5 PINOUT SETUP
 *                       -----------------------------
 *                         RST        OUTPUT    23
 *                         SCK        OUTPUT    18
 *                         SDA        OUTPUT    19
 *
 ***************************************************************************/

//////////////////////////////////////////////////////////
// Libraries
//////////////////////////////////////////////////////////

#include <i2c_t3.h>                               // Instead of #include <Wire.h>. More info: https://goo.gl/RyzFaM
#include <Adafruit_Sensor.h>                      // Unified Sensor Library
#include <Adafruit_BNO055_t3.h>                   // Instead of #include <Adafruit_BNO055.h>. More info: https://goo.gl/eAYKjn
#include <utility/imumaths.h>                     // Required to use vector and quaternion namespace

//////////////////////////////////////////////////////////
// Constants
//////////////////////////////////////////////////////////

#define BNO_RST                     (23)          //!< Teensy's pin to inertial unit **reset** pin
#define LED                         (13)          //!< Teensy's **LED** pin

//////////////////////////////////////////////////////////
// Class declaration
//////////////////////////////////////////////////////////

//! Instance of the IMU driver
/*! 
 * \param[in] iBus I2C Bus
 * \param[in] sensorID Sensor ID 
 * \param[in] aAddress Sensor I2C Address
 * \param[in] iMode I2C Mode (Master/Slave)
 * \param[in] pins I2C Pins
 * \param[in] pullup I2C Pull-ups (Internal/External)
 * \param[in] iRate I2C Rate [Hz]
 * \param[in] opeMode I2C Operational mode
 */

Adafruit_BNO055 bno = Adafruit_BNO055(WIRE_BUS, -1, BNO055_ADDRESS_A, I2C_MASTER,
                                      I2C_PINS_18_19, I2C_PULLUP_EXT, I2C_RATE_100, I2C_OP_MODE_ISR);

//////////////////////////////////////////////////////////
// Program Setup
//////////////////////////////////////////////////////////

void setup(void)
{
  Serial.begin(115200);
  while (!Serial)
  {
    digitalWrite(LED, HIGH);
    delay(200);
    digitalWrite(LED, LOW);
    delay(200);
  }
  delay(5000);
  Serial.println("Orientation Sensor Raw Data Test"); Serial.println("");

  /* Initialise the sensor */
  if (!bno.begin())
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    while (1);
  }
  else
  {
    Serial.print("BNO055 detected ... Setting up program");
  }

  delay(1000);

  int8_t temp = bno.getTemp();            // Get current temperature
  Serial.print("Current Temperature: ");
  Serial.print(temp);
  Serial.println(" C");
  Serial.println("");

  setCal();
  bno.setExtCrystalUse(true);
  bno.setMode(bno.OPERATION_MODE_NDOF);   // Set Operational mode
}

//////////////////////////////////////////////////////////
// Program Loop
//////////////////////////////////////////////////////////

void loop(void)
{
 
  /* Display calibration status for each sensor. */
  uint8_t system, gyro, accel, mag = 0;
  bno.getCalibration(&system, &gyro, &accel, &mag);
  Serial.print("CALIBRATION: Sys=");
  Serial.print(system, DEC);
  Serial.print(" Gyro=");
  Serial.print(gyro, DEC);
  Serial.print(" Accel=");
  Serial.print(accel, DEC);
  Serial.print(" Mag=");
  Serial.print(mag, DEC);

  /* Display IMU health */
  uint8_t status, result, error;
  bno.getSystemStatus(&status, &result, &error);
  Serial.print("\tSTATUS: Status=");
  Serial.print(status, DEC);
  Serial.print(" Result=");
  Serial.print(result, HEX);
  Serial.print(" Error=");
  Serial.println(error, DEC);
  

  delay(100);
}

//! Calibrate IMU setting predefined offsets
void setCal()
{
  const uint8_t calibrationData[22] = {uint8_t lowByte(65522),uint8_t highByte(65522),
                                    uint8_t lowByte(65499),uint8_t highByte(65499),
                                    uint8_t lowByte(27),   uint8_t highByte(27),
                                                
                                    uint8_t lowByte(65535),uint8_t highByte(65535),
                                    uint8_t lowByte(2),    uint8_t highByte(2),
                                    uint8_t lowByte(1),    uint8_t highByte(1),
                                                
                                    uint8_t lowByte(172),  uint8_t highByte(172),
                                    uint8_t lowByte(65316),uint8_t highByte(65316),
                                    uint8_t lowByte(155),  uint8_t highByte(155),
                                                
                                    uint8_t lowByte(1000), uint8_t highByte(1000),
                                    uint8_t lowByte(475),  uint8_t highByte(475)};
  
  bno.setSensorOffsets(calibrationData);
  delay(4000);
  Serial.println("IMU: offsets added");

  // Check if IMU is calibrated
  if (bno.isFullyCalibrated())
  {
      Serial.println("IMU: fully calibrated");
  } 
}
