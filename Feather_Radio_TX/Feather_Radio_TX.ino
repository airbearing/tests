/****************************************************************************
 * Air Bearing
 * David Criado Pernia
 * 2019
 * 
 * TEST
 * Feather M0 as a Radio transmitter of raw data
 *
 * Example sketch showing how to create a simple messageing client
 * with the RH_RF69 class. RH_RF69 class does not provide for addressing or
 * reliability, so you should only use RH_RF69  if you do not need the higher
 * level messaging abilities.
 * It is designed to work with the other example rf69_server.
 * Demonstrates the use of AES encryption, setting the frequency and modem
 * configuration
 * 
 ***************************************************************************/

//////////////////////////////////////////////////////////
// Libraries
//////////////////////////////////////////////////////////

#include <SPI.h>                    // Arduino
#include <RH_RF69.h>                // Third-party

//////////////////////////////////////////////////////////
// Constants
//////////////////////////////////////////////////////////

///////// Transceiver
#define RF69_FREQ (433.0)           //!< Transceiver Frequency [Hz]

///////// Feather M0
#define RFM69_CS        (8)         //!< Feather M0 pin to transceiver SPI chip select pin
#define RFM69_INT       (3)         //!< Feather M0 pin to transceiver SPI interrupt pin
#define RFM69_RST       (4)         //!< Feather M0 pin to transceiver SPI reset pin
#define LED             (13)        //!< Feather M0 LED pin

//////////////////////////////////////////////////////////
// Class declaration
//////////////////////////////////////////////////////////

RH_RF69 rf69(RFM69_CS, RFM69_INT);  //<! Instance of radio driver

//////////////////////////////////////////////////////////
// Program Setup
//////////////////////////////////////////////////////////

int16_t packetnum = 0;              //<! Packet counter

void setup()
{
  Serial.begin(115200);
  while (!Serial)
  {
    digitalWrite(LED, HIGH);
    delay(200);
    digitalWrite(LED, LOW);
    delay(200);
  }

  pinMode(LED, OUTPUT);
  pinMode(RFM69_RST, OUTPUT);
  digitalWrite(RFM69_RST, LOW);

  Serial.println("Feather RFM69 TX Test!");
  Serial.println();

  // manual reset
  digitalWrite(RFM69_RST, HIGH);
  delay(10);
  digitalWrite(RFM69_RST, LOW);
  delay(10);

  if (!rf69.init())
  {
    Serial.println("RFM69 radio init failed");
    while (1);
  }
  Serial.println("RFM69 radio init OK!");
  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM (for low power module)
  // No encryption
  if (!rf69.setFrequency(RF69_FREQ))
  {
    Serial.println("setFrequency failed");
  }

  // If you are using a high power RF69 eg RFM69HW, you *must* set a Tx power with the
  // ishighpowermodule flag set like this:
  rf69.setTxPower(20, true);  // range from 14-20 for power, 2nd arg must be true for 69HCW

  // The encryption key has to be the same as the one in the server
  uint8_t key[] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08
                  };
  rf69.setEncryptionKey(key);

  pinMode(LED, OUTPUT);

  Serial.print("RFM69 radio @");  Serial.print((int)RF69_FREQ);  Serial.println(" MHz");
}

//////////////////////////////////////////////////////////
// Program Loop
//////////////////////////////////////////////////////////

void loop()
{
  delay(1000);  // Wait 1 second between transmits, could also 'sleep' here!

  char radiopacket[20] = "Hello World #";
  itoa(packetnum++, radiopacket + 13, 10);
  Serial.print("Sending "); Serial.println(radiopacket);

  // Send a message!
  rf69.send((uint8_t *)radiopacket, strlen(radiopacket));
  rf69.waitPacketSent();

  // Now wait for a reply
  uint8_t buf[RH_RF69_MAX_MESSAGE_LEN];
  uint8_t len = sizeof(buf);

  if (rf69.waitAvailableTimeout(500))
  {
    // Should be a reply message for us now
    if (rf69.recv(buf, &len))
    {
      Serial.print("Got a reply: ");
      Serial.println((char*)buf);
      blink(LED, 50, 3); // blink LED 3 times, 50ms between blinks
    }
    else
    {
      Serial.println("Receive failed");
    }
  }
  else
  {
    Serial.println("No reply, is another RFM69 listening?");
  }
}

//! Blink pin
void blink(byte PIN, byte DELAY_MS, byte loops)
{
  for (byte i = 0; i < loops; i++)
  {
    digitalWrite(PIN, HIGH);
    delay(DELAY_MS);
    digitalWrite(PIN, LOW);
    delay(DELAY_MS);
  }
}
