/****************************************************************************
 * Air Bearing
 * David Criado Pernia
 * 2019
 * 
 * TEST
 * Teensy Serial USB communications
 *
 * If serial does not work, blink fast. Otherwise, prompt serial message
 * and blink slow.
 * 
 ***************************************************************************/

//////////////////////////////////////////////////////////
// Constants
//////////////////////////////////////////////////////////

#define LED             (13)        //!< Teensy M0 LED pin

//////////////////////////////////////////////////////////
// Program Setup
//////////////////////////////////////////////////////////

void setup()
{
  pinMode(LED, OUTPUT);
  Serial.begin(115200);
  while (!Serial)
  {
    digitalWrite(LED, HIGH);
    delay(200);
    digitalWrite(LED, LOW);
    delay(200);
  }
}

//////////////////////////////////////////////////////////
// Program Loop
//////////////////////////////////////////////////////////

void loop()
{
  Serial.println("Teensy Serial works");
  digitalWrite(LED, HIGH);
  delay(1000);
  digitalWrite(LED, LOW);
  delay(1000);
}
