/****************************************************************************
 * Air Bearing
 * David Criado Pernia
 * 2019
 * 
 * TEST
 * Teensy and transceiver as a transmitter of packetized data
 *
 * Example sketch showing how to create a simple addressed, reliable messaging client
 * with the RH_RF69 class. RH_RF69 class does not provide for addressing or
 * reliability, so you should only use RH_RF69  if you do not need the higher
 * level messaging abilities.
 * Demonstrates the use of AES encryption, setting the frequency and modem 
 * configuration
 *
 * 
 *                          TEENSY 3.5 PINOUT SETUP
 *                       -----------------------------
 *                         RST        OUTPUT    23
 *                         SCK        OUTPUT    18
 *                         SDA        OUTPUT    19
 *                         RST        OUTPUT    34
 *                         G0 (IRQ)   INPUT     33
 *                         CS         OUTPUT    15 (CS0)
 *                         MOSI       -         28 (MOSI0)
 *                         MISO       -         39 (MISO0)
 *                         SCK        -         14 (SCK0)
 *                         EN (NO CONNECTED)
 *                         GND        -         GND
 *                         VIN        -         3.3V
 * 
 ***************************************************************************/

//////////////////////////////////////////////////////////
// Libraries
//////////////////////////////////////////////////////////

#include <SPI.h>                // SPI
#include <RH_RF69.h>            // Radio driver
#include <RHReliableDatagram.h> // Packetized coms

//////////////////////////////////////////////////////////
// Constants
//////////////////////////////////////////////////////////

#define RF69_FREQ                   (433.0)       //!< Transceiver Frequency [Hz]

#define DEST_ADDRESS                (1)           //!< **Station** transceiver Address: where to send packets to
#define MY_ADDRESS                  (2)           //!< **Platform** transceiver Address: platform's own adress

#define RFM69_RST                   (34)          //!< Teensy's pin to transceiver SPI **reset** pin
#define LED                         (13)          //!< Teensy's **LED** pin
#define RFM69_INT                   (33)          //!< Teensy's pin to transceiver SPI **interrupt** pin
#define RFM69_CS                    (15)          //!< Teensy's pin to transceiver SPI **chip select** pin
#define RFM69_SCK                   (14)          //!< Teensy's pin to transceiver SPI **clock** pin
#define RFM69_MOSI                  (28)          //!< Teensy's pin to transceiver SPI **MOSI** pin
#define RFM69_MISO                  (39)          //!< Teensy's pin to transceiver SPI **MISO** pin

//////////////////////////////////////////////////////////
// Class declaration
//////////////////////////////////////////////////////////

RH_RF69 rf69(RFM69_CS, RFM69_INT);                 //<! Radio driver
RHReliableDatagram rf69_manager(rf69, MY_ADDRESS); //<! Manage message delivery and receipt

//////////////////////////////////////////////////////////
// Program Setup
//////////////////////////////////////////////////////////

int16_t packetnum = 0;                //<! Packet counter
uint8_t data[] = "TM ACK";            //<! telemetry ACK message
uint8_t buf[RH_RF69_MAX_MESSAGE_LEN]; //<! Buffer to receive

void setup()
{
  Serial.begin(115200);
  while (!Serial)
  {
    digitalWrite(LED, HIGH);
    delay(200);
    digitalWrite(LED, LOW);
    delay(200);
  }

  SPI.begin();
  SPI.setSCK(RFM69_SCK);
  SPI.setMOSI(RFM69_MOSI);
  SPI.setMISO(RFM69_MISO);

  pinMode(LED, OUTPUT);     
  pinMode(RFM69_RST, OUTPUT);
  digitalWrite(RFM69_RST, LOW);

  Serial.println("Feather Addressed RFM69 TX Test!");
  Serial.println();

  // manual reset
  digitalWrite(RFM69_RST, HIGH);
  delay(10);
  digitalWrite(RFM69_RST, LOW);
  delay(10);
  
  if (!rf69_manager.init())
  {
    Serial.println("RFM69 radio init failed");
    while (1);
  }
  Serial.println("RFM69 radio init OK!");
  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM (for low power module)
  // No encryption
  if (!rf69.setFrequency(RF69_FREQ))
  {
    Serial.println("setFrequency failed");
  }

  // If you are using a high power RF69 eg RFM69HW, you *must* set a Tx power with the
  // ishighpowermodule flag set like this:
  rf69.setTxPower(20, true);  // range from 14-20 for power, 2nd arg must be true for 69HCW

  // The encryption key has to be the same as the one in the server
  uint8_t key[] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
  rf69.setEncryptionKey(key);
  
  pinMode(LED, OUTPUT);

  Serial.print("RFM69 radio @");  Serial.print((int)RF69_FREQ);  Serial.println(" MHz");
}

//////////////////////////////////////////////////////////
// Program Loop
//////////////////////////////////////////////////////////

void loop() {
  delay(1000);  // Wait 1 second between transmits, could also 'sleep' here!

  char radiopacket[20] = "Hello World #";
  itoa(packetnum++, radiopacket+13, 10);
  Serial.print("Sending "); Serial.println(radiopacket);
  
  // Send a message to the DESTINATION!
  if (rf69_manager.sendtoWait((uint8_t *)radiopacket, strlen(radiopacket), DEST_ADDRESS))
  {
    // Now wait for a reply from the server
    uint8_t len = sizeof(buf);
    uint8_t from;   
    if (rf69_manager.recvfromAckTimeout(buf, &len, 2000, &from))
    {
      buf[len] = 0; // zero out remaining string
      
      Serial.print("Got reply from #"); Serial.print(from);
      Serial.print(" [RSSI :");
      Serial.print(rf69.lastRssi());
      Serial.print("] : ");
      Serial.println((char*)buf);     
      blink(LED, 40, 3); // blink LED 3 times, 40ms between blinks
    }
    else
    {
      Serial.println("No reply, is anyone listening?");
    }
  }
  else
  {
    Serial.println("Sending failed (no ack)");
  }
}

//! Blink pin
void blink(byte PIN, byte DELAY_MS, byte loops)
{
  for (byte i=0; i<loops; i++)
  {
    digitalWrite(PIN,HIGH);
    delay(DELAY_MS);
    digitalWrite(PIN,LOW);
    delay(DELAY_MS);
  }
}
